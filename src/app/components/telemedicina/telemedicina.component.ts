import { Component, OnInit } from '@angular/core';
import { TelemedService } from '../../services/telemed.service'

@Component({
  selector: 'app-telemedicina',
  templateUrl: './telemedicina.component.html',
  styleUrls: ['./telemedicina.component.css']
})
export class TelemedicinaComponent implements OnInit {

  token = '';

  constructor(private telemedService : TelemedService ) { }

  ngOnInit() {
  }
  
  autenticar(){
    this.telemedService.autenticar().subscribe( res => {
      this.token = res.access_token;
    });
  }

  cadastrarResponsavel(nomeBeneficiario,email,cpf,telefone,carteirinha,sexo,dataNascimento){
    console.log("Cadastrou" + nomeBeneficiario + email+ cpf+ telefone+ carteirinha + sexo+ dataNascimento)
  }

}
