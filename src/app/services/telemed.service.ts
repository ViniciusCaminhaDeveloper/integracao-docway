import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class TelemedService {

  constructor(private http: HttpClient) { }

  autenticar() : Observable<any>  {


    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', 'Basic c2FzYXBwOnNlY3JldA==');
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');

    let grant_type = 'client_credentials';
    let body = `grant_type=${grant_type}`;
    
    return this.http.post('https://api.docway.com.br/auth/connect/token', body, {headers: headers});
  }
}
