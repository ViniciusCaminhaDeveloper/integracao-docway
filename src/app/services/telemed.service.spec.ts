import { TestBed } from '@angular/core/testing';

import { TelemedService } from './telemed.service';

describe('TelemedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TelemedService = TestBed.get(TelemedService);
    expect(service).toBeTruthy();
  });
});
